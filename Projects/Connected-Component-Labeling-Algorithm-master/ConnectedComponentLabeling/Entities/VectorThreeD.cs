﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnectedComponentLabeling
{
    public class VectorThreeD
    {
        #region public properties

        public int X;
        public int Y;
        public int Z;

        #endregion

        #region Contructor(s)

        public VectorThreeD(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        #endregion
    }
}
