﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ConnectedComponentLabeling
{
    public class Pixel
    {
        #region Public Properties

        public VectorThreeD Position { get; set; }
        public double dose { get; set; }

        #endregion

        #region Constructor

        public Pixel(VectorThreeD Position, double dose)
        {
            this.Position = Position;
            this.dose = dose;
        }

        #endregion
    }
}