﻿using System.Collections.Generic;
using System.Drawing;

namespace ConnectedComponentLabeling
{
    public interface IConnectedComponentLabeling
    {
        IDictionary<int, double[,,]> Process(double[,,] input, double cutoff);

        // GLSZM method where I only need the size of the blobs
        IDictionary<int, int> ProcessReturnOnlySize(double[,,] input, double cutoff);

        // Method returns list of pixels instead of double image
        IDictionary<int, List<Pixel>> ProcessReturnPixels(double[,,] input, double cutoff);
    }
}