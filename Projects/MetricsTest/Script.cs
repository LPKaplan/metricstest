using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.Generic;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace VMS.TPS
{
  public class Script
  {
        public Script()
        {
        }

        public void Execute(ScriptContext context /*, System.Windows.Window window*/)
        {
            Patient patient = context.Patient;
            patient.BeginModifications();

            // Conformity
            //var evalConf = new MetricsTest.TestConformity(context.PlansInScope.First(p => p.Id == "Conf_0"), context.PlansInScope.First(p => p.Id == "Conf_1"), context.PlansInScope.First(p => p.Id == "Conf_2"));
            //MessageBox.Show("Choose save location for overcoverage test:");
            //evalConf.PrintOverCovResults();
            //MessageBox.Show("Choose save location for undercoverage test:");
            //evalConf.PrintUnderCovResults();
            //MessageBox.Show("Choose save location for moved coverage test:");
            //evalConf.PrintMovedCovResults();

            ////Gradient
            var evalGrad = new MetricsTest.TestGradient(context.PlansInScope.First(p => p.Id == "Grad_0"), context.PlansInScope.First(p => p.Id == "Grad_1"), context.PlansInScope.First(p => p.Id == "Grad_2"), context.PlansInScope.First(p => p.Id == "Grad_3"));
            //MessageBox.Show("Choose save location for isotropic expansion test:");
            //evalGrad.PrintResults100();
            //MessageBox.Show("Choose save location for 75% expansion test:");
            //evalGrad.PrintResults75();
            //MessageBox.Show("Choose save location for 50% expansion test:");
            //evalGrad.PrintResults50();
            MessageBox.Show("Coose save location for directional expansion test:");
            evalGrad.PrintResultsDirection();

            //// Homogeneity
            //var evalHom = new MetricsTest.TestHomogeneity(context.PlansInScope.First(p => p.Id == "Hom"));
            //MessageBox.Show("Choose save location for homogeneity test:");
            //evalHom.PrintResults();

            ////Location
            //var evalLoc = new MetricsTest.TestLocation(context.PlansInScope.First(p => p.Id == "Location"), context.PlansInScope.First(p => p.Id == "Location1"), context.PlansInScope.First(p => p.Id == "Location2"), context.PlansInScope.First(p => p.Id == "Location3"), context.Image);
            //MessageBox.Show("Choose save location for location test:");
            //evalLoc.PrintResults();

            //// Distribution
            //var evalDist = new MetricsTest.TestDistribution(context.PlansInScope.First(p => p.Id == "Distribution"), context.Image);
            //MessageBox.Show("Choose save location for distribution test:");
            //evalDist.PrintResults();
        }
  }
}
