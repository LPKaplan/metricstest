﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using OxyPlot;
using ConnectedComponentLabeling;

namespace MetricsTest
{
    public static class DistributionMetrics
    {
        public static double CalculateLCSd(PlanSetup plan, Structure structure, Structure isodose, Image image)
        // This function calculates a penalty for large cold spots in the target volume. It is the same as the LCS metric proposed by Said et al
        // Interpolate to uniform grid?
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;
            var binaryDoseMatrix = GetDoseThreeDFromIsodose(isodose, structure, image, 1, 1, 1);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, 66); // Arbitrary cutoff value is not used here

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is the blob's label as key and its size (in voxels) as value
            { 
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }


        // Helper methods
        

        // Helper methods
        /// <summary>
        /// Gets the voxelwise dose values from the dose object and returns them as a 3D array
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <returns>3D double array of voxel dose values. Voxels outside the structure are set to 0.</returns>
        public static double[,,] GetDoseThreeDFromIsodose(Structure isodose, Structure structure, Image image, double xres, double yres, double zres)
        {
            // number of voxels
            //int xcount = image.XSize;
            //int ycount = image.YSize;
            //int zcount = image.ZSize;
            int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
            int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
            int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            System.Collections.BitArray isodoseSegmentStride = new System.Collections.BitArray(xcount);
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * yres +
                                    image.ZDirection * z * zres;
                    VVector end = start + image.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    SegmentProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = isodose.GetSegmentProfile(start, end, isodoseSegmentStride);
                            }

                            doseMatrix[i, y, z] = Convert.ToDouble(doseProfile[i].Value);
                            
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        
    }
}
