﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;
using ConnectedComponentLabeling;

namespace MetricsTest
{
    class TestDistribution
    {
        private readonly PlanSetup Plan;
        private readonly Image Image;

        public TestDistribution(PlanSetup plan, Image image)
        {
            this.Plan = plan;
            this.Image = image;
        }

        public void PrintResults()
        {
            string path = GetSavePath();

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                double xres = 1;
                double yres = 1;
                double zres = 1;

                Structure PTV = Plan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");

                // title line
                sw.WriteLine("Scenario;LCS_D;Number of cold spots above 0.1cc;Cold spot sizes");

                // One cold spot
                sw.Write("One cold spot;");
                Structure Cold1 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold");

                sw.Write(DistributionMetrics.CalculateLCSd(Plan, PTV, Cold1, Image).ToString() + ";");

                var oneCSDoseMatrix = DistributionMetrics.GetDoseThreeDFromIsodose(Cold1, PTV, Image, xres, yres, zres);
                var oneCSCCL = new BinaryCCL();
                var oneCS = oneCSCCL.ProcessReturnOnlySize(oneCSDoseMatrix, 66); // Cutoff value is arbitrary and not used


                sw.Write(oneCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                foreach (var cs in oneCS)
                {
                    sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                }
                sw.WriteLine();

                // Two cold spots
                sw.Write("Two cold spots;");
                Structure Cold2 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold1");

                sw.Write(DistributionMetrics.CalculateLCSd(Plan, PTV, Cold2, Image).ToString() + ";");

                var twoCSDoseMatrix = DistributionMetrics.GetDoseThreeDFromIsodose(Cold2, PTV, Image, xres, yres, zres);
                var twoCSCCL = new BinaryCCL();
                var twoCS = oneCSCCL.ProcessReturnOnlySize(twoCSDoseMatrix, 66); // Cutoff value is arbitrary and not used
                sw.Write(twoCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                foreach (var cs in twoCS)
                {
                    sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                }
                sw.WriteLine();

                // Many cold spots
                sw.Write("Many cold spots;");
                Structure Cold3 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold2");

                sw.Write(DistributionMetrics.CalculateLCSd(Plan, PTV, Cold3, Image).ToString() + ";");

                var manyCSDoseMatrix = DistributionMetrics.GetDoseThreeDFromIsodose(Cold3, PTV, Image, xres, yres, zres);
                var manyCSCCL = new BinaryCCL();
                var manyCS = oneCSCCL.ProcessReturnOnlySize(manyCSDoseMatrix, 66); // Cutoff value is arbitrary and not used
                sw.Write(manyCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                foreach (var cs in manyCS)
                {
                    sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                }
                sw.WriteLine();
            }
        }

        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for distribution test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
