﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;
using Accord.Collections;

namespace MetricsTest
{
    public static class GradientMetrics
    {
        public static double GI_Paddick(Structure refIsodose, Structure halfRefIsodose)
        {
            return halfRefIsodose.Volume / refIsodose.Volume;
        }

        public static double GI_Mayo(Structure refIsodose, Structure halfRefIsodose)
        {
            double Reff50 = Math.Pow(3.0 / 4.0 * halfRefIsodose.Volume / Math.PI, 1.0 / 3.0);
            double Reff100 = Math.Pow(3.0 / 4.0 * refIsodose.Volume / Math.PI, 1.0 / 3.0);

            return 50 / (Reff50 - Reff100); // unit: percent per cm
        }

        public static double GI_Wagner(Structure refIsodose, Structure halfRefIsodose)
        {
            double Reff50 = Math.Pow(3.0 / 4.0 * halfRefIsodose.Volume / Math.PI, 1.0 / 3.0);
            double Reff100 = Math.Pow(3.0 / 4.0 * refIsodose.Volume / Math.PI, 1.0 / 3.0);

            return  (Reff50 - Reff100);
        }

        // isodose 1 has lower dose value than isodose2, to get positive values.
        public static double GI_SungChoi(Structure isodose2, Structure isodose1)
        {
            double S1 = GetSurfaceArea(isodose1);
            double S2 = GetSurfaceArea(isodose2);

            return (isodose1.Volume - isodose2.Volume) / (0.5 * (S1 + S2));
        }

        public static double GI_Ohtakara(Structure target, Structure isodose)
        {
            return (isodose.Volume / target.Volume);
        }

        public static double FindDirectionalGradient(Structure target, Structure isodose, Structure OAR, string direction)
        {
            // Eclipse GUI coordinate system: 
            //  x: LR. Left: positive direction
            //  y: AP. Posterior: positive direction
            //  z: CC. Cranial: positive direction


            // start point: target center-of-mass
            VVector COM = target.CenterPoint;

            // take average over five adjacent lines to increase robustness. Place lines in a "cross-hair" arrangement.
            List<VVector> startPoints = new List<VVector>();
            List<VVector> endPoints = new List<VVector>();

            // initialize variables
            double dist = 0;
            double count = 0;
            VVector pointOnLine = new VVector(0,0,0); // dummy initial value. will get overwritten or ignored
            VVector isoPoint = new VVector(0,0,0); // dummy initial value. will get overwritten or ignored

            if (direction.ToUpper() == "ANTERIOR")
            {
                // define start points to loop over
                for (double j = -1; j <= 1; j += 1)
                {
                    if (j == 0)
                    {
                        for (double k = -1; k <= 1; k += 1)
                        {
                            startPoints.Add(new VVector(COM.x + k, COM.y, COM.z));
                        }
                    }
                    else
                    {
                        startPoints.Add(new VVector(COM.x, COM.y, COM.z + j));
                    }
                }
                
                // Loop over start points
                foreach (var startPoint in startPoints)
                {
                    // end point 10 cm anterior of start point
                    VVector endPoint = new VVector(startPoint.x, startPoint.y - 100, startPoint.z);
                    endPoints.Add(endPoint);

                    // initiate buffer for segment profile
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray(1000); // resolution : 0.1mm
                    System.Collections.BitArray segmentBufferIso = new System.Collections.BitArray(1000); // resolution : 0.1mm

                    // get segment profile
                    var segment = target.GetSegmentProfile(startPoint, endPoint, segmentBuffer);
                    var isoSegment = isodose.GetSegmentProfile(startPoint, endPoint, segmentBufferIso);

                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (!segment[i].Value) // when edge of the structure is reached. For these structures, COM is always inside structure. For general case, this routine should be adapted to account for the case where COM is outside the structure.
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found.
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (int i = 0; i < isoSegment.Count(); i++)
                    {
                        if (!isoSegment[i].Value) // when edge of the structure is reached. For these structures, COM is always inside structure. For general case, this routine should be adapted to account for the case where COM is outside the structure.
                        {
                            isoPoint = isoSegment[i].Position;
                            dist += GetDistance3D(isoPoint, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }


                return dist / count;
            }

            else if (direction.ToUpper() == "RIGHT")
            {
                // define start points to loop over
                for (double j = -1; j <= 1; j += 1)
                {
                    if (j == 0)
                    {
                        for (double k = -1; k <= 1; k += 1)
                        {
                            startPoints.Add(new VVector(COM.x, COM.y + k, COM.z));
                        }
                    }
                    else
                    {
                        startPoints.Add(new VVector(COM.x, COM.y, COM.z + j));
                    }
                }
                
                // Loop over start points
                foreach (var startPoint in startPoints)
                {
                    // end point 10 cm anterior of start point
                    VVector endPoint = new VVector(startPoint.x - 100, startPoint.y, startPoint.z);

                    // initiate buffer for segment profile
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray(1000); // resolution : 0.1mm
                    System.Collections.BitArray segmentBufferIso = new System.Collections.BitArray(1000); // resolution : 0.1mm

                    // get segment profile
                    var segment = target.GetSegmentProfile(startPoint, endPoint, segmentBuffer);
                    var isoSegment = isodose.GetSegmentProfile(startPoint, endPoint, segmentBufferIso);

                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (!segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found.
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (int i = 0; i < isoSegment.Count(); i++)
                    {
                        if (!isoSegment[i].Value)
                        {
                            isoPoint = isoSegment[i].Position;
                            dist += GetDistance3D(isoPoint, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }

                return dist / count;
            }

            else if (direction.ToUpper() == "OAR")
            {
                // define start points to loop over

                // find plane orthogonal to targetCOM to OAR-COM line
                VVector COMaxis = new VVector(OAR.CenterPoint.x - COM.x, OAR.CenterPoint.y - COM.y, OAR.CenterPoint.z - COM.z);
                VVector unitVector = COMaxis / COMaxis.Length;

                // find random vector in the plane
                VVector randomPlanePoint = new VVector(COM.x, COM.y + 5 / unitVector.y, COM.z - 5 / unitVector.z);
                VVector planeVector1 = randomPlanePoint - COM;
                VVector planeUnitVector1 = planeVector1 / planeVector1.Length;

                // find second plane vector. Cross product of unit normal vector and unit first plane vector gives orthogonal unit vector
                VVector planeVector2 = new VVector(unitVector.y * planeUnitVector1.z - unitVector.z * planeUnitVector1.y, unitVector.z * planeUnitVector1.x - unitVector.x * planeUnitVector1.z, unitVector.x * planeUnitVector1.y - unitVector.y * planeUnitVector1.x);
                VVector planeUnitVector2 = planeVector2 / planeVector2.Length;

                for (double j = -1; j <= 1; j += 1)
                {
                    if (j == 0)
                    {
                        for (double k = -1; k <= 1; k += 1)
                        {
                            startPoints.Add(new VVector(COM.x + (k * planeUnitVector1.x), COM.y + (k * planeUnitVector1.y), COM.z + (k * planeUnitVector1.z)));
                        }
                    }
                    else
                    {
                        startPoints.Add(new VVector(COM.x + (j * planeUnitVector2.x), COM.y + (j * planeUnitVector2.x), COM.z + (j * planeUnitVector2.x)));
                    }
                }
                
                // Loop over start points
                foreach (var startPoint in startPoints)
                {
                    // end point 10 cm anterior of start point
                    VVector endPoint = new VVector(startPoint.x + (100 * unitVector.x), startPoint.y + (100 * unitVector.y), startPoint.z + (100 * unitVector.z));

                    // initiate buffer for segment profile
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray(1000); // resolution : 0.1mm
                    System.Collections.BitArray segmentBufferIso = new System.Collections.BitArray(1000); // resolution : 0.1mm

                    // get segment profile
                    var segment = target.GetSegmentProfile(startPoint, endPoint, segmentBuffer);
                    var isoSegment = isodose.GetSegmentProfile(startPoint, endPoint, segmentBufferIso);

                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (!segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found.
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (int i = 0; i < isoSegment.Count(); i++)
                    {
                        if (!isoSegment[i].Value)
                        {
                            isoPoint = isoSegment[i].Position;
                            dist += GetDistance3D(isoPoint, pointOnLine);
                            count += 1;
                            break;
                        }
                    }
                }

                return dist / count;
            }

            else
            {
                return double.NaN;
            }
        }

        public static Tuple<double, double> FindOARGradient(Structure OAR, Structure isodose)
        {
            // initialize lists of points
            List<double[]> treePointsList = new List<double[]>();
            List<double[]> queryPointsList = new List<double[]>();

            // Initialize list to hold all min distances
            List<double> distances = new List<double>();

            // Isodose points go in the tree. OAR points are query points.
            foreach (var point in isodose.MeshGeometry.Positions)
            {
                treePointsList.Add(new double[] { point.X, point.Y, point.Z });
            }
            
            foreach (var point in OAR.MeshGeometry.Positions)
            {
                queryPointsList.Add(new double[] { point.X, point.Y, point.Z });
            }
            
            // populate tree
            KDTree<double> tree = KDTree.FromData<double>(treePointsList.ToArray());

            // Find shortest distance for each query point
            foreach (var qPoint in queryPointsList)
            {
                var neighbors = tree.Nearest(qPoint, neighbors: 1);

                distances.Add(neighbors.First().Distance);
            }

            // return shortest distance and average over shortest 10%
            distances.Sort();
            return new Tuple<double, double>( distances.Min(), distances.Take((int)Math.Floor(distances.Count() * 0.1)).Average());
        }

        public static List<Tuple<double,double>> FindDirectionalGradientPerSlice(PlanSetup plan, Structure structure, Structure isodose, string direction)
        {
            // initialize list
            List<Tuple<double, double>> retList = new List<Tuple<double, double>>();

            // loop over CC slices
            for (int z = 0; z < plan.Dose.ZSize; z++)
            {
                // find gradient only if structure is delineated on the plane
                if (IsStructureOnImagePlane(structure, z))
                {
                    // Get coordinate value of plane nr z
                    double zCoor = plan.Dose.Origin.z + z * plan.Dose.ZRes;

                    // Anterior
                    if (direction == "Anterior")
                    {
                        // use COM.x as example search line location
                        double centerX = structure.CenterPoint.x;

                        // initialize vector objects for structure and isodose contour points
                        VVector structureContourPoint = new VVector();
                        VVector isodoseContourPoint = new VVector();

                        // initialize distance and count variables
                        double dist = 0;
                        double count = 0;

                        // average over five adjacent lines
                        for (double k = -1; k <= 1; k += 0.5)
                        {

                            // Find most anterior point of the structure on this line
                            // start and end points for segments
                            VVector start = new VVector(centerX, plan.Dose.Origin.y, zCoor);
                            VVector end = new VVector(centerX, plan.Dose.Origin.y + (plan.Dose.YSize * plan.Dose.YRes), zCoor);

                            // Buffers for structure segments
                            System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes) * 10); // resolution : ca 0.1mm
                            System.Collections.BitArray isodoseSegmentBuffer = new System.Collections.BitArray((int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes) * 10); // resolution : ca 0.1mm


                            // Get segments
                            SegmentProfile segment = structure.GetSegmentProfile(start, end, segmentBuffer);
                            SegmentProfile isodoseSegment = isodose.GetSegmentProfile(start, end, isodoseSegmentBuffer);

                            // does structure contour lie on the line?
                            bool found = false;
                            bool isoFound = false;

                            // Search segment until anterior point of contour is found
                            for (int i = 0; i < segment.Count(); i++)
                            {
                                if (segment[i].Value)
                                {
                                    structureContourPoint = segment[i].Position;
                                    found = true;
                                    break;
                                }
                            }
                            for (int i = 0; i < isodoseSegment.Count(); i++)
                            {
                                if (isodoseSegment[i].Value)
                                {
                                    isodoseContourPoint = isodoseSegment[i].Position;
                                    isoFound = true;
                                    break;
                                }
                            }

                            // if either structure or isodose are not on the search line, return nan and move to next slice
                            if (found & isoFound)
                            {
                                dist += GetDistance2D(structureContourPoint, isodoseContourPoint);
                                count += 1;
                            }
                        }

                        if (count != 0)
                        {
                            retList.Add(new Tuple<double, double>(zCoor, dist / count));
                        }
                        else
                        {
                            retList.Add(new Tuple<double, double>(zCoor, double.NaN));
                        }
                    }

                    // Right
                    else if (direction == "Right")
                    {
                        // use COM.y as example search line location
                        double centerY = structure.CenterPoint.y;

                        // initialize vector objects for structure and isodose contour points
                        VVector structureContourPoint = new VVector();
                        VVector isodoseContourPoint = new VVector();

                        double dist = 0;
                        double count = 0;

                        //average over three adjacent lines
                        for (double k = -1; k <= 1; k += 1)
                        {

                            // Find most anterior point of the structure on this line
                            // start and end points for segments
                            VVector start = new VVector(plan.Dose.Origin.x, centerY, zCoor);
                            VVector end = new VVector(plan.Dose.Origin.x + (plan.Dose.XSize * plan.Dose.XRes), centerY, zCoor);

                            // Buffers for structure segments
                            System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes) * 10); // resolution : ca 0.1mm
                            System.Collections.BitArray isodoseSegmentBuffer = new System.Collections.BitArray((int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes) * 10); // resolution : ca 0.1mm


                            // Get segments
                            SegmentProfile segment = structure.GetSegmentProfile(start, end, segmentBuffer);
                            SegmentProfile isodoseSegment = isodose.GetSegmentProfile(start, end, isodoseSegmentBuffer);

                            // does structure contour lie on the line?
                            bool found = false;
                            bool isoFound = false;

                            // Search segment until anterior point of contour is found
                            for (int i = 0; i < segment.Count(); i++)
                            {
                                if (segment[i].Value)
                                {
                                    structureContourPoint = segment[i].Position;
                                    found = true;
                                    break;
                                }
                            }
                            for (int i = 0; i < isodoseSegment.Count(); i++)
                            {
                                if (isodoseSegment[i].Value)
                                {
                                    isodoseContourPoint = isodoseSegment[i].Position;
                                    isoFound = true;
                                    break;
                                }
                            }

                            if (found & isoFound)
                            {
                                dist += GetDistance2D(structureContourPoint, isodoseContourPoint);
                                count += 1;
                            }
                        }

                        // if either structure or isodose are not on the search line, return nan and move to next slice
                        if (count != 0)
                        {
                            retList.Add(new Tuple<double, double>(zCoor, dist / count));
                        }
                        else
                        {
                            retList.Add(new Tuple<double, double>(zCoor, double.NaN));
                        }
                    }
                }
            }

            return retList;
        }

        //public static double CalculateMeanDirGradient(PlanSetup plan, Structure structure, int direction) // 0=x, 1=y, 2=z
        //{

        //}

        // Helper methods
        private static double GetSurfaceArea(Structure structure)
        {
            var points = structure.MeshGeometry.Positions;
            var triangleIndices = structure.MeshGeometry.TriangleIndices;
            double area = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                Point3D a = points.ElementAt(triangleIndices.ElementAt(i * 3));
                Point3D b = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                Point3D c = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));

                Vector3D ab = new Vector3D(c.X - a.X, c.Y - a.Y, c.Z - a.Z);
                Vector3D ac = new Vector3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);

                double abDOTac = (ab.X * ac.X) + (ab.Y * ac.Y) + (ab.Z * ac.Z);

                if (ab.Length > 0 && ac.Length > 0)
                {
                    var val = 0.5 * ab.Length * ac.Length * Math.Sqrt(1 - Math.Pow(abDOTac / (ab.Length * ac.Length), 2));
                    if (!double.IsNaN(val))
                        area += val;
                }
            }
            return area / 100;
        }

        private static double GetDistance2D(VVector point1, VVector point2)
        {
            return Math.Sqrt(Math.Pow((point1.x - point2.x), 2) + Math.Pow((point1.y - point2.y), 2));
        }

        private static double GetDistance3D(VVector point1, VVector point2)
        {
            return Math.Sqrt(Math.Pow((point1.x - point2.x), 2) + Math.Pow((point1.y - point2.y), 2) + Math.Pow((point1.z - point2.z), 2));
        }

        private static bool IsStructureOnImagePlane(Structure structure, int z)
        {
            // initialize return variable
            bool retVar = false;

            // get contours
            VVector[][] contours = structure.GetContoursOnImagePlane(z + 1); // There is a bug in the ESAPI method here from what I can tell. GetContoursOnImagePlane(z) returns contours for z+1.

            // are there contours on this plane?
            if (contours.Length > 0)
            {
                retVar = true;
            }

            return retVar;
        }

        // for debugging
        private static string GetTxtPath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose txt file save location for gradient test",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
