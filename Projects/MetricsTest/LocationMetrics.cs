﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using OxyPlot;

namespace MetricsTest
{
    public static class LocationMetrics
    {
        /// <summary>
        /// Returns spatial DVH for a given structure. The spatial DVH is derived by cropping the main structure with the crop structure.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cropStructure"></param>
        /// <param name="margins">Margins to crop with.</param>
        /// <returns></returns>
        public static DVHPoint[][] GetSpatialDvh(PlanSetup plan, Structure structure, Structure cropStructure, List<double> margins)
        {
            // sort in ascending order
            margins.Sort();

            // initialize array of dvhpoint arrays
            DVHPoint[][] returnArray = new DVHPoint[margins.Count() + 1][];

            if (plan.StructureSet.CanAddStructure("CONTROL", structure.Id + "_near"))
            {
                SegmentVolume cropVolMid = cropStructure.Margin(margins.ElementAt(0));
                SegmentVolume cropVolFar = cropStructure.Margin(margins.ElementAt(1));
                SegmentVolume structFarSegment = structure.Sub(cropVolFar);
                SegmentVolume structMidSegment = structure.Sub(cropVolMid).Sub(structFarSegment);
                SegmentVolume structNearSegment = structure.Sub(structMidSegment).Sub(structFarSegment);

                Structure OARnear = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_near");
                OARnear.SegmentVolume = structNearSegment;
                Structure OARmid = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_mid");
                OARmid.SegmentVolume = structMidSegment;
                Structure OARfar = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_far");
                OARfar.SegmentVolume = structFarSegment;

                if (!OARnear.IsEmpty)
                {
                    DVHData nearDVH = plan.GetDVHCumulativeData(OARnear, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[0] = nearDVH.CurveData;
                }

                if (!OARmid.IsEmpty)
                {
                    DVHData midDVH = plan.GetDVHCumulativeData(OARmid, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[1] = midDVH.CurveData;
                }

                if (!OARfar.IsEmpty)
                {
                    DVHData farDVH = plan.GetDVHCumulativeData(OARfar, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[2] = farDVH.CurveData;
                }

                plan.StructureSet.RemoveStructure(OARnear);
                plan.StructureSet.RemoveStructure(OARmid);
                plan.StructureSet.RemoveStructure(OARfar);
            }
            return returnArray;
        }

        // spatial DVH
        /// <summary>
        /// Returns spatial DVH for a given structure. The spatial DVH is taken as "shells" of the structure. Returns DVH sequence from outer to inner.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="margins">Margins for the shells. Give the distance from structure edge to each shell. Only three volumes can be calculated: two margin inputs needed..</param>
        /// <returns></returns>
        public static DVHPoint[][] GetSpatialDvh(PlanSetup plan, Structure structure, List<double> margins)
        {
            // sort in ascending order
            margins.Sort();

            // initialize array of dvhpoint arrays
            DVHPoint[][] returnArray = new DVHPoint[margins.Count()][];
            if (plan.StructureSet.CanAddStructure("CONTROL", structure.Id + "_inner"))
            {
                SegmentVolume cropVolMid = structure.Margin(-margins.ElementAt(0));
                SegmentVolume cropVolInner = structure.Margin(-margins.ElementAt(1));
                SegmentVolume structFarSegment = cropVolInner;
                SegmentVolume structMidSegment = cropVolMid.Sub(structFarSegment);
                SegmentVolume structNearSegment = structure.Sub(structMidSegment).Sub(structFarSegment);

                Structure OARnear = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_outer");
                OARnear.SegmentVolume = structNearSegment;
                Structure OARmid = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_mid");
                OARmid.SegmentVolume = structMidSegment;
                Structure OARfar = plan.StructureSet.AddStructure("CONTROL", structure.Id + "_inner");
                OARfar.SegmentVolume = structFarSegment;

                if (!OARnear.IsEmpty)
                {
                    DVHData nearDVH = plan.GetDVHCumulativeData(OARnear, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[0] = nearDVH.CurveData;
                }

                if (!OARmid.IsEmpty)
                {
                    DVHData midDVH = plan.GetDVHCumulativeData(OARmid, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[1] = midDVH.CurveData;
                }

                if (!OARfar.IsEmpty)
                {
                    DVHData farDVH = plan.GetDVHCumulativeData(OARfar, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    returnArray[2] = farDVH.CurveData;
                }

                plan.StructureSet.RemoveStructure(OARnear);
                plan.StructureSet.RemoveStructure(OARmid);
                plan.StructureSet.RemoveStructure(OARfar);
            }


            return returnArray;
        }


        public static List<DataPoint> GetColdOVH(PlanSetup plan, Structure structure, Image image, double cutoff)
        {
            List<VVector> structurePoints = FindPointsInsideStructure(plan, image, structure);
            List<double> distances = new List<double>();
            List<DataPoint> points = new List<DataPoint>();

            foreach (var point in structurePoints)
            {
                double pointDose = plan.Dose.GetDoseToPoint(point).Dose;

                if (pointDose < cutoff)
                {
                    var dist = FindStructureEdgeDist(point, image, structure);
                    distances.Add(dist);
                }
            }

            if (distances.Count() == 0)
            {
                return points;
            }

            for (double i = 0; i < distances.Max() + 1; i += 0.1)
            {
                // each distance corresponds to one voxel. This will slightly overestimate the total volume because voxels might partly extend beyond the structure, and because DVH dose sampling may be different.
                double vol = distances.Count(d => d <= i) * image.XRes * image.YRes * image.ZRes / 1000;
                var p = new DataPoint(i, vol);
                points.Add(p);
            }

            return points;
        }

        public static List<DataPoint> GetHotOVH(PlanSetup plan, Structure structure1, Structure structure2, Image image, double cutoff)
        {
            var structurePoints = FindPointsInsideStructure(plan, image, structure1);
            List<double> distances = new List<double>();
            List<DataPoint> points = new List<DataPoint>();

            foreach (var point in structurePoints)
            {
                double pointDose = plan.Dose.GetDoseToPoint(point).Dose;

                if (pointDose > cutoff)
                {
                    var dist = FindStructureEdgeDist(point, image, structure2);

                    if (structure2.IsPointInsideSegment(point)) // assign negative distance to point inside target
                    {
                        distances.Add(-dist);
                    }
                    else
                    {
                        distances.Add(dist);
                    }

                }
            }


            if (distances.Count() == 0)
            {
                return points;
            }

            for (double i = distances.Min(); i < distances.Max() + 1; i += 0.1) // min value: target's effective radius
            {
                double vol = distances.Count(d => d <= i) * image.XRes * image.YRes * image.ZRes / 1000;
                var p = new DataPoint(i, vol);
                points.Add(p);
            }
            return points;
        }

        // Helper methods

        /// <summary>
        /// Calculate the 3D distance between two points.
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        private static double GetDistance3D(Point3D point1, Point3D point2)
        {
            double dist = Math.Sqrt(Math.Pow((double)point1.X - (double)point2.X, 2) + Math.Pow((double)point1.Y - (double)point2.Y, 2) + Math.Pow((double)point1.Z - (double)point2.Z, 2));
            return dist;
        }

        /// <summary>
        /// Calculate the 3D distance between two points.
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        private static double GetDistance3D(VVector point1, VVector point2)
        {
            double dist = Math.Sqrt(Math.Pow((double)point1.x - (double)point2.x, 2) + Math.Pow((double)point1.y - (double)point2.y, 2) + Math.Pow((double)point1.z - (double)point2.z, 2));
            return dist;
        }

        /// <summary>
        /// Method to find shortest distance from a given point to a structure's contour. Points inside a given structure can be found with the method FindPointsInsideStructure().
        /// </summary>
        /// <param name="point"></param>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private static double FindStructureEdgeDist(VVector point, Image image, Structure structure)
        {
            List<double> allDist = new List<double>();

            foreach (var meshPoint in structure.MeshGeometry.Positions)
            {
                allDist.Add(GetDistance3D(point, Point3DToVVector(meshPoint)));
            }

            return allDist.Min();
        }

        // Convert System.Windows.Medie.Media3D.Point3D to VMS VVector
        private static VVector Point3DToVVector(Point3D point)
        {
            return new VVector(point.X, point.Y, point.Z);
        }

        /// <summary>
        /// Method to get a list of all grid points (VVector objects) inside a given structure
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="image"></param>
        /// <param name="structure"></param>
        /// <returns>List of VVectors points</returns>
        private static List<VVector> FindPointsInsideStructure(PlanSetup plan, Image image, Structure structure)
        {
            Dose dose = plan.Dose;
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(image.XSize);

            List<VVector> points = new List<VVector>();



            for (int z = 0; z < image.ZSize; z += 1)
            {
                for (int y = 0; y < image.YSize; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * image.YRes +
                                    image.ZDirection * z * image.ZRes;
                    VVector end = start + image.XDirection * image.XRes * image.XSize;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            points.Add(segmentProfile[i].Position);
                        }
                    }
                }
            }
            return points;
        }
    }
}
