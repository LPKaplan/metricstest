﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;

namespace MetricsTest
{
    class TestGradient
    {
        private readonly PlanSetup Plan100;
        private readonly PlanSetup Plan75;
        private readonly PlanSetup Plan50;
        private readonly PlanSetup PlanDir;

        public TestGradient(PlanSetup plan100, PlanSetup plan75, PlanSetup plan50, PlanSetup planDir)
        {
            this.Plan100 = plan100;
            this.Plan75 = plan75;
            this.Plan50 = plan50;
            this.PlanDir = planDir;
        }

        public void PrintResults100()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;GI Paddick;GI Wagner;GI Ohtakara;GI Mayo;GCI");

                // 1 cm to 50% isodose
                sw.WriteLine("1cm distance");
                Structure sph_31 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+1cm");
                Structure sph_51 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+1cm");
                Structure sph_71 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+1cm");
                Structure lng1 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng+1cm");
                Structure irreg1 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg+1cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng1).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 2 cm to 50% isodose
                sw.WriteLine("2cm distance");
                Structure sph_32 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+2cm");
                Structure sph_52 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+2cm");
                Structure sph_72 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+2cm");
                Structure lng2 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng+2cm");
                Structure irreg2 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg+2cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng2).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 3 cm to 50% isodose
                sw.WriteLine("3cm distance");
                Structure sph_33 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+3cm");
                Structure sph_53 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+3cm");
                Structure sph_73 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+3cm");
                Structure lng3 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng+3cm");
                Structure irreg3 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg+3cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng3).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
            }
        }

        public void PrintResults75()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;GI Paddick;GI Wagner;GI Ohtakara;GI Mayo;GCI");

                // 1 cm to 50% isodose
                sw.WriteLine("1cm distance");
                Structure sph_31 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+1cm");
                Structure sph_51 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+1cm");
                Structure sph_71 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+1cm");
                Structure lng1 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng+1cm");
                Structure irreg1 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg+1cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng1).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 2 cm to 50% isodose
                sw.WriteLine("2cm distance");
                Structure sph_32 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+2cm");
                Structure sph_52 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+2cm");
                Structure sph_72 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+2cm");
                Structure lng2 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng+2cm");
                Structure irreg2 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg+2cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng2).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 3 cm to 50% isodose
                sw.WriteLine("3cm distance");
                Structure sph_33 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+3cm");
                Structure sph_53 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+3cm");
                Structure sph_73 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+3cm");
                Structure lng3 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng+3cm");
                Structure irreg3 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg+3cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng3).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
            }
        }

        public void PrintResults50()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;GI Paddick;GI Wagner;GI Ohtakara;GI Mayo;GCI");

                // 1 cm to 50% isodose
                sw.WriteLine("1cm distance");
                Structure sph_31 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+1cm");
                Structure sph_51 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+1cm");
                Structure sph_71 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+1cm");
                Structure lng1 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng+1cm");
                Structure irreg1 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg+1cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng1).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 2 cm to 50% isodose
                sw.WriteLine("2cm distance");
                Structure sph_32 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+2cm");
                Structure sph_52 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+2cm");
                Structure sph_72 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+2cm");
                Structure lng2 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng+2cm");
                Structure irreg2 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg+2cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng2).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 3 cm to 50% isodose
                sw.WriteLine("3cm distance");
                Structure sph_33 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+3cm");
                Structure sph_53 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+3cm");
                Structure sph_73 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+3cm");
                Structure lng3 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng+3cm");
                Structure irreg3 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg+3cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng3).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
            }
        }

        public void PrintResultsDirection()
        {
            string path = GetSavePath();

            Structure sph7 = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm");
            Structure sph7_iso = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso");
            Structure sph7_ant = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_ant");
            Structure sph7_right = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_right");
            Structure sph7_oar = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR");

            Structure irreg = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg");
            Structure irreg_iso = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_iso");
            Structure irreg_ant = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_ant");
            Structure irreg_right = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_right");
            Structure irreg_oar = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_OAR");

            Structure OAR = this.PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "OAR");

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("Target shape;GI_SungChoi;GI_Anterior;GI_Right;GI_OAR;GI_OAR_shortest;GI_OAR_avg");

                sw.WriteLine("Isotropic;");

                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_iso).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso, OAR, "OAR"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso).Item2.ToString());
                sw.Write(";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_iso).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_iso, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_iso, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_iso, OAR, "OAR"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_iso).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_iso).Item2.ToString());
                sw.Write(";");
                sw.WriteLine();

                sw.WriteLine("Anterior bulge;");

                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_ant).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_ant, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_ant, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_ant, OAR, "OAR").ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_ant).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_ant).Item2.ToString());
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_ant).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_ant, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_ant, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_ant, OAR, "OAR"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_ant).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_ant).Item2.ToString());
                sw.WriteLine();

                sw.WriteLine("Right bulge;");

                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_right).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "OAR"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_right).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_right).Item2.ToString());
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_right).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_right, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_right, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_right, OAR, "OAR"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_right).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_right).Item2.ToString());
                sw.WriteLine();

                sw.WriteLine("OAR bulge;");

                Structure sph7_oar1 = PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR1");
                Structure sph7_oar2 = PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR2");
                Structure sph7_oar3 = PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR3");
                Structure sph7_oar4 = PlanDir.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR4");

                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_oar).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_oar, OAR, "Anterior"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_oar, OAR, "Right"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_oar, OAR, "OAR"));
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar1).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar1).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar2).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar2).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar3).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar3).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar4).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar4).Item2.ToString());
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_oar).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_oar, OAR, "Anterior").ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_oar, OAR, "Right").ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_oar, OAR, "OAR").ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_oar).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_oar).Item2.ToString());
                sw.WriteLine();

                // Get 2D anterior and Right gradient indices along CC axis
                List<Tuple<double, double>> antAntList = GradientMetrics.FindDirectionalGradientPerSlice(PlanDir, sph7, sph7_ant, "Anterior");
                List<Tuple<double, double>> rightAntList = GradientMetrics.FindDirectionalGradientPerSlice(PlanDir, sph7, sph7_ant, "Right");
                List<Tuple<double, double>> antRightList = GradientMetrics.FindDirectionalGradientPerSlice(PlanDir, sph7, sph7_right, "Anterior");
                List<Tuple<double, double>> rightRightList = GradientMetrics.FindDirectionalGradientPerSlice(PlanDir, sph7, sph7_right, "Right");

                sw.WriteLine("Anterior bulge, anterior gradient");
                foreach (Tuple<double, double> point in antAntList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Anterior bulge, right gradient");
                foreach (Tuple<double, double> point in rightAntList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Right bulge, anterior gradient");
                foreach (Tuple<double, double> point in antRightList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Right bulge, Right gradient");
                foreach (Tuple<double, double> point in rightRightList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();
            }
        }

        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for gradient test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
